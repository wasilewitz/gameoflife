#ifndef __GRID_HPP__
#define __GRID_HPP__

#include <vector>
#include "Cell.hpp"
#include <SFML\Graphics.hpp>
#include "GameState.hpp"
#include "Config.hpp"

class Grid : public sf::Drawable
{
public:
    Grid( uint32_t columnsCount, uint32_t rowsCount );
    void Simulate( GameState gameState );
    Cell& GetCell( int column, int row ) { return m_cells[column][row]; }
    void UpdateView( sf::RenderWindow& window );

private:
    virtual void draw( sf::RenderTarget& target, sf::RenderStates states ) const;

    std::vector<std::vector<Cell>> m_cells;
    sf::VertexArray m_vertices;
    uint32_t m_rowsCount;
    uint32_t m_columnsCount;
    int m_threadsNum;
};

#endif // !__GRID_HPP__
