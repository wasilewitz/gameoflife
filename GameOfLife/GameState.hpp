#ifndef __GAME_STATE_HPP__
#define __GAME_STATE_HPP__

enum GameState { START, SIMULATION, END };

#endif // !__GAME_STATE_HPP__
