#include <SFML/Graphics.hpp>
#include "Grid.hpp"
#include <iostream>
#include "Overlay.hpp"

int main()
{
    sf::RenderWindow window( sf::VideoMode( WINDOW_WIDTH, WINDOW_HEIGHT ), "Game of Life - OpenMP" );
    GameState gameState = GameState::START;
    Grid grid( COLUMNS_COUNT, ROWS_COUNT );
    uint32_t simulationRefreshmentId = 0;
    sf::Clock rendererClock;
    sf::Clock simulationClock;
    sf::Clock keyboardClock;
    Overlay overlay;

    while( window.isOpen() )
    {
        bool simulate = simulationClock.getElapsedTime().asMilliseconds() >= SIMULATION_REFRESHMENT_DT[simulationRefreshmentId];
        if( simulate )
        {
            simulationClock.restart();
            grid.Simulate( gameState );
        }

        bool render = rendererClock.getElapsedTime().asMilliseconds() >= RENDER_REFRESHMENT_DT;
        if( render )
        {
            sf::Event event;
            while( window.pollEvent( event ) )
            {
                if( event.type == sf::Event::Closed )
                {
                    gameState = GameState::END;
                    window.close();
                }
            }

            if( gameState == GameState::START )
            {
                if( sf::Keyboard::isKeyPressed( sf::Keyboard::Key::Space ) )
                {
                    gameState = GameState::SIMULATION;
                    continue;
                }
            }
            else if( gameState == GameState::SIMULATION && keyboardClock.getElapsedTime().asMilliseconds() > 150 )
            {
                if( sf::Keyboard::isKeyPressed( sf::Keyboard::Key::Left ) )
                {
                    keyboardClock.restart();
                    simulationRefreshmentId = simulationRefreshmentId == 0 ? SIMULATION_REFRESHMENT_DT.size() - 1 : simulationRefreshmentId - 1;
                }
                else if( sf::Keyboard::isKeyPressed( sf::Keyboard::Key::Right ) )
                {
                    keyboardClock.restart();
                    simulationRefreshmentId = simulationRefreshmentId == SIMULATION_REFRESHMENT_DT.size() - 1 ? simulationRefreshmentId = 0 : simulationRefreshmentId + 1;
                }
            }

            rendererClock.restart();
            window.clear();
            overlay.UpdateView( gameState, simulationRefreshmentId );
            grid.UpdateView( window );
            window.draw( grid );
            window.draw( overlay );
            window.display();
        }
    }

    return 0;
}