#include "Grid.hpp"
#include <iostream>
#include <omp.h>

static const sf::Color hoveredCellColor = sf::Color( 192, 192, 192 );
static const sf::Color deadCellColor = sf::Color( 34, 34, 34 );
static const sf::Color aliveCellColor = sf::Color::White;

Grid::Grid( uint32_t columnsCount, uint32_t rowsCount )
    : m_columnsCount( columnsCount )
    , m_rowsCount( rowsCount )
{
    m_threadsNum = omp_get_thread_num();

    m_cells.resize( columnsCount );
    for( auto& row : m_cells )
    {
        row.resize( rowsCount );
    }

    m_vertices.setPrimitiveType( sf::Quads );
    m_vertices.resize( rowsCount * columnsCount * 4 );

    #pragma omp parallel for num_threads(m_threadsNum) 
    for( int columnId = 0; columnId < columnsCount; ++columnId )
    {
        for( int rowId = 0; rowId < rowsCount; ++rowId )
        {
            sf::Vertex* quad = &m_vertices[( columnId + rowId * columnsCount ) * 4];

            quad[0].position = sf::Vector2f( columnId * CELL_SIZE + ( columnId + 1 ) * SEPARATOR_SIZE, rowId * CELL_SIZE + ( rowId + 1 ) * SEPARATOR_SIZE );
            quad[1].position = sf::Vector2f( ( columnId + 1 ) * CELL_SIZE + ( columnId + 1 ) * SEPARATOR_SIZE, rowId * CELL_SIZE + ( rowId + 1 ) * SEPARATOR_SIZE );
            quad[2].position = sf::Vector2f( ( columnId + 1 ) * CELL_SIZE + ( columnId + 1 ) * SEPARATOR_SIZE, ( rowId + 1 ) * CELL_SIZE + ( rowId + 1 ) * SEPARATOR_SIZE );
            quad[3].position = sf::Vector2f( columnId * CELL_SIZE + ( columnId + 1 ) * SEPARATOR_SIZE, ( rowId + 1 ) * CELL_SIZE + ( rowId + 1 ) * SEPARATOR_SIZE );


            for( int i = 0; i < 4; ++i )
            {
                quad[i].color = deadCellColor;
            }
        }
    }
}

void Grid::Simulate( GameState gameState )
{
    if( gameState != GameState::SIMULATION )
    {
        return;
    }

    const std::vector<std::pair<int, int>> neighboursOffsets{
        { -1, 0 },
        { -1, -1 },
        { 0, -1 },
        { 1, -1 },
        { 1, 0 },
        { 1, 1 },
        { 0, 1 },
        { -1, 1 }
    };

    #pragma omp parallel for num_threads(m_threadsNum) 
    for( int columnId = 0; columnId < m_columnsCount; ++columnId )
    {
        for( int rowId = 0; rowId < m_rowsCount; ++rowId )
        {
            auto& currentCell = m_cells[columnId][rowId];
            currentCell.Invalidate();
        }
    }

    #pragma omp parallel for num_threads(m_threadsNum) 
    for( int columnId = 0; columnId < m_columnsCount; ++columnId )
    {
        for( int rowId = 0; rowId < m_rowsCount; ++rowId )
        {
            auto& currentCell = m_cells[columnId][rowId];
            int aliveNeighbours = 0;
            for( auto& offset : neighboursOffsets )
            {
                int neighbourColumn = columnId + offset.first;
                int neighbourRow = rowId + offset.second;
                if( neighbourColumn >= 0 && neighbourColumn < m_columnsCount && neighbourRow >= 0 && neighbourRow < m_rowsCount )
                {
                    auto& neighbourCell = m_cells[neighbourColumn][neighbourRow];
                    if( neighbourCell.IsAlive() )
                    {
                        aliveNeighbours++;
                    }
                }
            }
            currentCell.CheckLifeState( aliveNeighbours );
        }
    }
}

void Grid::UpdateView( sf::RenderWindow& window )
{
    auto& mousePos = sf::Mouse::getPosition( window );

    int hoveredColumn = static_cast<float>( mousePos.x ) / ( SEPARATOR_SIZE + CELL_SIZE );
    int hoveredRow = static_cast<float>( mousePos.y ) / ( SEPARATOR_SIZE + CELL_SIZE );
    bool mouseInWindow = hoveredColumn >= 0 && hoveredColumn < m_columnsCount && hoveredRow >= 0 && hoveredRow < m_rowsCount;

    #pragma omp parallel for num_threads(m_threadsNum) 
    for( int columnId = 0; columnId < m_columnsCount; ++columnId )
    {
        for( int rowId = 0; rowId < m_rowsCount; ++rowId )
        {
            sf::Vertex* quad = &m_vertices[( columnId + rowId * m_columnsCount ) * 4];
            bool isHovered = mouseInWindow && columnId == hoveredColumn && rowId == hoveredRow;
            sf::Color notHoveredCellColor = GetCell( columnId, rowId ).IsAlive() ? aliveCellColor : deadCellColor;
            sf::Color cellColor = isHovered ? hoveredCellColor : notHoveredCellColor;
            for( int i = 0; i < 4; ++i )
            {
                quad[i].color = cellColor;
            }
        }
    }

    if( sf::Mouse::isButtonPressed( sf::Mouse::Button::Left ) && mouseInWindow )
    {
        auto& cell = GetCell( hoveredColumn, hoveredRow );
        if( !cell.IsAlive() )
        {
            cell.SetAlive( true );
        }
    }
}

void Grid::draw( sf::RenderTarget & target, sf::RenderStates states ) const
{
    target.draw( m_vertices, states );
}
