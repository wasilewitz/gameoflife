#include <stdint.h>
#include <vector>

static const uint32_t WINDOW_WIDTH = 1280;
static const uint32_t WINDOW_HEIGHT = 720;
static const uint32_t CELL_SIZE = 8;
static const uint32_t SEPARATOR_SIZE = 1;

static const uint32_t COLUMNS_COUNT = WINDOW_WIDTH / ( SEPARATOR_SIZE + CELL_SIZE ) + 1;
static const uint32_t ROWS_COUNT = WINDOW_HEIGHT / ( SEPARATOR_SIZE + CELL_SIZE ) + 1;

static const std::vector<uint32_t> SIMULATION_REFRESHMENT_DT{ 512, 256, 64, 16 };
static const uint32_t RENDER_REFRESHMENT_DT = 16;