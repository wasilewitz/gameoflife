#include "Cell.hpp"

void Cell::CheckLifeState( int aliveNeighbours )
{
    if( IsAlive() )
    {
        m_aliveInNextStep = aliveNeighbours == 2 || aliveNeighbours == 3;
    }
    else if( !IsAlive() )
    {
        m_aliveInNextStep = aliveNeighbours == 3;
    }
}
