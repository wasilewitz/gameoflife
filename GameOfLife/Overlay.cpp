#include "Overlay.hpp"
#include <cassert>
#include "Config.hpp"

Overlay::Overlay()
    : m_simulationRefreshmentSpeedId( 0 )
    , m_gameState( GameState::START )
{
    bool fontLoaded = m_font.loadFromFile( "arial.ttf" );
    assert( fontLoaded );
}

void Overlay::UpdateView( GameState gameState, uint32_t simulationRefreshmentSpeedId )
{
    m_gameState = gameState;
    m_simulationRefreshmentSpeedId = simulationRefreshmentSpeedId;
}

void Overlay::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    sf::RectangleShape overlayRect( sf::Vector2f( static_cast<float>( WINDOW_WIDTH ), 100.0f ) );
    overlayRect.setFillColor( sf::Color( 0, 0, 0, 140 ) );

    target.draw( overlayRect );

    if( m_gameState == GameState::START )
    {
        sf::Text text( "Set some cells alive, by clicking on them and press Space to start the simulation.", m_font );
        target.draw( text );
    }
    else
    {
        sf::Text text1( "Click left arrow or right arrow to change simulation speed.", m_font );
        sf::Text text2( "Simulation speed: " + std::to_string( m_simulationRefreshmentSpeedId + 1 ) + " / " + std::to_string( SIMULATION_REFRESHMENT_DT.size() ) + " - refresh every " + std::to_string( SIMULATION_REFRESHMENT_DT[m_simulationRefreshmentSpeedId] ) + "ms", m_font );
        text2.setPosition( 0, 40 );
        target.draw( text1 );
        target.draw( text2 );
    }
}
