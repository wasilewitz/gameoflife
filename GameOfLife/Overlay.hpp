#ifndef __OVERLAY_HPP__
#define __OVERLAY_HPP__

#include <SFML\Graphics.hpp>
#include "GameState.hpp"

class Overlay : public sf::Drawable
{
public:
    Overlay();
    void UpdateView( GameState gameState, uint32_t simulationRefreshmentSpeedId );
private:
    virtual void draw( sf::RenderTarget& target, sf::RenderStates states ) const;
    sf::Font m_font;
    uint32_t m_simulationRefreshmentSpeedId;
    GameState m_gameState;
};

#endif // !__OVERLAY_HPP__
