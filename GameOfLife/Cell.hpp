#ifndef __CELL_HPP__
#define __CELL_HPP__

class Cell
{
public:
    Cell()
        : m_isAlive( false )
        , m_aliveInNextStep( false )
    {}

    bool IsAlive() { return m_isAlive; } const
    void Invalidate() { m_isAlive = m_aliveInNextStep; }
    void CheckLifeState( int aliveNeighbours );
    void SetAlive( bool alive ) { m_isAlive = alive; m_aliveInNextStep = true; }
private:

    bool m_isAlive;
    bool m_aliveInNextStep;
};

#endif // !__CELL_HPP__
